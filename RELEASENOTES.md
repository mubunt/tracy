# RELEASE NOTES:  *tracy*, simple C trace macros with output to a file.

Functional limitations, if any, of this version are described in the *README.md* file.

- **Versopn 1.0.3:**
  - Added timestamp in traces.

- **Versopn 1.0.2:**
  - Removed one trailing space in header file.

- **Versopn 1.0.1:**
  - Replaced license files (COPYNG, LICENSE and RELEASENOTES) by markdown version.
  - Moved from GPL v2 to GPL v3.

- **Version 1.0.0**:
    - First release.
