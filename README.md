# *tracy*,  simple C trace macros with output to a file.
***
**tracy** is a very simple set of macros to ease the adding of traces to programs to debug. The result of traces will be in the *tracy.log* text file, located in current directory.

The source cod of the application, with these tracing primitives, can remain unchanged in release mode by not predefining the TRACY macro.

To use **tracy**:
   - include the *tracy.h* header file in all files that need to trace any information.
   - in one of the files (usually the main, but not necessarily), define the file descriptor that will be used (macro *TRACYFD_DEFINITION*) in a section of global variables.
   - compile the sources of the application by predefining the *TRACY* macro 

## LICENSE
**tracy** is covered by the GNU General Public License (GPL) version 3 and above.

## BASIC EXAMPLE
### Programs
``` c
#include <stdio.h>
#include <unistd.h>
#include <tracy.h>

TRACYFD_DEFINITION;

extern int f1(int);

int main( void ) {
	int t = 10;
	int c;
	_TRACY_START();
	sleep(2);
	_TRACY_PRINT("The value of t is %d", t);
	_TRACY_PRINT("f1 returns %d", f1(t));
	sleep(1);
	_TRACY_STOP();
	return 0;
}
```

``` c
#include <stdio.h>
#include <tracy.h>

int f1(int n) {
	int sq = n * 2;
	_TRACY_PRINT("The value of n is %d", n);
	_TRACY_PRINT("I return %d", sq);
	return sq;
}
```
### Build and Run
``` bash
$ gcc -DTRACY -I. -o example main.c sub.c
$ ./example
```
### Output file
``` bash
$ cat tracy.log 
1581008911 | main.c           | 0012 | main             | Beginning of Traces
1581008913 | main.c           | 0014 | main             | The value of t is 10
1581008913 | sub.c            | 0006 | f1               | The value of n is 10
1581008913 | sub.c            | 0007 | f1               | I return 20
1581008913 | main.c           | 0015 | main             | f1 returns 20
1581008914 | main.c           | 0017 | main             | End of Traces
$
```
## SOFTWARE REQUIREMENTS
- For usage and development: *none*
- Developped and tested on XUBUNTU 19.04, GCC v8.3.0

## RELEASE NOTES
Refer to file [RELEASENOTES](./RELEASENOTES.md).

***